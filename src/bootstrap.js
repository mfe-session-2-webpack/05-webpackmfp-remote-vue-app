import { createApp } from 'vue';
import Hello from './Hello.vue';

// Mount function to start up the app
const mount = (el) => {
  const app = createApp(Hello);
  app.mount(el);
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#app');
  if (devRoot) {
    mount(devRoot);
  }
}

export { mount };
