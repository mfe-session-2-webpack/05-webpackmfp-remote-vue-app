const htmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;
const { VueLoaderPlugin } = require('vue-loader');

const { dependencies } = require('../package.json');

module.exports = {
  entry: './src/bootstrap.js',
  output: {
    clean: true,
    chunkFilename: '[name].js',
  },
  resolve: {
    extensions: ['.vue', '.jsx', '.js', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              presets: ['@babel/react', '@babel/env'],
            },
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new htmlWebpackPlugin({
      template: './index.html',
    }),
    new ModuleFederationPlugin({
      name: 'RemoteVue',
      filename: 'remoteEntry.js',
      exposes: {
        './AppRemoteVue': './src/bootstrap',
      },
      shared: {
        ...dependencies,
        vue: {
          singleton: true,
          eager: true,
          requiredVersion: dependencies.vue,
        },
      },
    }),
  ],
};
