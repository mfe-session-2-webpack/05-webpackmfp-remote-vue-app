const path = require('path');

module.exports = {
  devtool: 'inline-source-map',
  devServer: {
    port: 4205,
    static: {
      directory: path.join(__dirname, './dist'),
    },
  },
};
